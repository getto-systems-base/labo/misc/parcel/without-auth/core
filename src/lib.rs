pub fn greet(name: &str) -> String {
    format!("Hello, {}!", name)
}

#[cfg(test)]
mod tests {
    use super::greet;

    #[test]
    fn it_works() {
        assert_eq!(greet("World"), "Hello, World!".to_string());
    }
}
